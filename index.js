/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk-core');
const request = require("request-promise");

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Bonjour, je suis Alexa, je vais vous poser des questions et ainsi évaluer votre personnalité, pouvons nous commencer ?';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const BeginningIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'BeginningIntent';
  },
  handle(handlerInput) {
    const speechText = 'Pour commencer, quel est votre nom ?';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const NameIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'NameIntent';
  },

  async handle(handlerInput) {

    const slots = handlerInput.requestEnvelope.request.intent.slots;
    const nom = slots['name'].value;
    const nom2 = slots['lastname'].value;

    const data = JSON.stringify({ name: nom, lastname: nom2 });
    var cookieJar = request.jar();

    request.post('http://3.91.81.58:8080/utilisateur', {
      jar: cookieJar,
      headers: { 'Content-Type': data, 'Content-Length': data.length }

    }, (error, res, body) => {
      if (error) {
        console.error(error);
        return;
      }
      console.log(`statusCode: ${res.statusCode}`);
      console.log(body);
    });

    let outputSpeech = 'This is the default message.';

    await new Promise(done => setTimeout(done, 1500));
    await request.get('http://3.91.81.58:8080/utilisateur', {
      jar: cookieJar,
      headers: { 'Content-Type': 'application/json' }

    }, (error, res, body) => {

      let data = JSON.parse(body);
      outputSpeech = `Très bien ${data.name} ${data.lastname}, pour commencer dites commencer questionnaire`;

      if (error) {
        console.error(error);
        return;
      }
      console.log(`statusCode: ${res.statusCode}`);
      console.log(body);
    });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(outputSpeech)
      .withSimpleCard('Hello World', outputSpeech)
      .getResponse();
  },
};

const GetRemoteDataHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'TestIntent';

  },
  async handle(handlerInput) {
    let outputSpeech = 'This is the default message.';

    await request.get('http://3.91.81.58:8080/questionnaire', {
      headers: { 'Content-Type': 'application/json' }

    }, (error, res, body) => {

      const data = JSON.parse(body);
      for (let i = 0; i < data.lq.length; i++) {
        //donnees.push(data.lq[i].intitule);
        outputSpeech = 'Question suivante ' + data.lq[i].intitule + ',';

      }

      return handlerInput.responseBuilder
        .speak(outputSpeech)
        .reprompt()
        .getResponse();
    });
    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .getResponse();
  },
};

const GetRemoteDataHandler2 = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'TestIntentt';

  },
  async handle(handlerInput) {
    let outputSpeech = 'This is the default message.';

    await request.get('http://3.91.81.58:8080/questionnaire', {
      headers: { 'Content-Type': 'application/json' }

    }, (error, res, body) => {

      const data = JSON.parse(body);
      for (let i = 0; i < data.lq.length; i++) {
        //donnees.push(data.lq[i].intitule);
        outputSpeech = 'Question suivante ' + data.lq[i].intitule + ',';

      }

      return handlerInput.responseBuilder
        .speak(outputSpeech)
        .reprompt()
        .getResponse();
    });
    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .getResponse();
  },
};

const ResultIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'ResultIntent';
  },

  async handle(handlerInput) {

    var cookieJar = request.jar();

    request.post('http://3.91.81.58:8080/reponses', {
      jar: cookieJar,
    }, (error, res, body) => {
      if (error) {
        console.error(error);
        return;
      }
      console.log(`statusCode: ${res.statusCode}`);
      console.log(body);
    });

    let outputSpeech = 'This is the default message.';

    await new Promise(done => setTimeout(done, 1500));
    await request.get('http://3.91.81.58:8080/reponses', {
      jar: cookieJar,
      headers: { 'Content-Type': 'application/json' }

    }, (error, res, body) => {

      let data = JSON.parse(body);
      outputSpeech = `Très bien, ${data.name}, merci d'avoir participer et à bientot je l'éspère !`;

      if (error) {
        console.error(error);
        return;
      }
      console.log(`statusCode: ${res.statusCode}`);
      console.log(body);
    });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .reprompt(outputSpeech)
      .withSimpleCard('Hello World', outputSpeech)
      .withShouldEndSession(true)
      .getResponse();
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent' ||
        handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Très bien, au revoir !';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    BeginningIntentHandler,
    NameIntentHandler,
    GetRemoteDataHandler,
    GetRemoteDataHandler2,
    ResultIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
